import { Server, ServerCredentials } from "@grpc/grpc-js";
import { ProjectsService, XormsService } from "./grpc/protos/models/projects_grpc_pb";
import { Projects } from "./grpc/services/projects";
import { Xorms } from "./grpc/services/xorms";

export default function serve(): void {
  const server = new Server();

  server.addService(ProjectsService, new Projects());
  server.addService(XormsService, new Xorms());
  server.bindAsync(`0.0.0.0:50051`, ServerCredentials.createInsecure(), (err, port) => {
      if (err) {
          throw err;
      }
      console.log(`gRPC server listening on port ${port}`);
      server.start();
  });
}
