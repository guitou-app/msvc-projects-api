import { Connection } from 'amqplib';
import * as amqp from 'amqplib';

class RabbitmqWrapper {
  private _connection?: Connection; 

  connect() {
    const { RABBITMQ_URI } = process.env;
    if (!RABBITMQ_URI) {
      throw new Error(`RABBITMQ URI must be defined`);
    }

    return new Promise<void>((resolve, reject) => {
      amqp.connect(RABBITMQ_URI)
      .then((conn: Connection) => {
        this._connection = conn;

        resolve();
      })
      .catch((error) => reject(error));
      //  , (err: any, conn: Connection) => {
      //   if (err) {
      //     reject();
      //     return;
      //   }
      //   this._connection = conn;
      //   resolve();
      // })
    })
  }

  disconnect() {

  }

  get connection() {
    return this._connection;
  }
}

export const rabbitmqWrapper = new RabbitmqWrapper();
