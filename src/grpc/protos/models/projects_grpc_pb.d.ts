// package: projects
// file: projects.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as projects_pb from "./projects_pb";

interface IProjectsService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    isProjectExists: IProjectsService_IisProjectExists;
    getAllUserProjects: IProjectsService_IgetAllUserProjects;
}

interface IProjectsService_IisProjectExists extends grpc.MethodDefinition<projects_pb.IDRequest, projects_pb.Project> {
    path: "/projects.Projects/isProjectExists";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<projects_pb.IDRequest>;
    requestDeserialize: grpc.deserialize<projects_pb.IDRequest>;
    responseSerialize: grpc.serialize<projects_pb.Project>;
    responseDeserialize: grpc.deserialize<projects_pb.Project>;
}
interface IProjectsService_IgetAllUserProjects extends grpc.MethodDefinition<projects_pb.IDRequest, projects_pb.Project> {
    path: "/projects.Projects/getAllUserProjects";
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<projects_pb.IDRequest>;
    requestDeserialize: grpc.deserialize<projects_pb.IDRequest>;
    responseSerialize: grpc.serialize<projects_pb.Project>;
    responseDeserialize: grpc.deserialize<projects_pb.Project>;
}

export const ProjectsService: IProjectsService;

export interface IProjectsServer extends grpc.UntypedServiceImplementation {
    isProjectExists: grpc.handleUnaryCall<projects_pb.IDRequest, projects_pb.Project>;
    getAllUserProjects: grpc.handleServerStreamingCall<projects_pb.IDRequest, projects_pb.Project>;
}

export interface IProjectsClient {
    isProjectExists(request: projects_pb.IDRequest, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    isProjectExists(request: projects_pb.IDRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    isProjectExists(request: projects_pb.IDRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    getAllUserProjects(request: projects_pb.IDRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<projects_pb.Project>;
    getAllUserProjects(request: projects_pb.IDRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<projects_pb.Project>;
}

export class ProjectsClient extends grpc.Client implements IProjectsClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public isProjectExists(request: projects_pb.IDRequest, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    public isProjectExists(request: projects_pb.IDRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    public isProjectExists(request: projects_pb.IDRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: projects_pb.Project) => void): grpc.ClientUnaryCall;
    public getAllUserProjects(request: projects_pb.IDRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<projects_pb.Project>;
    public getAllUserProjects(request: projects_pb.IDRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<projects_pb.Project>;
}

interface IXormsService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getXormById: IXormsService_IgetXormById;
}

interface IXormsService_IgetXormById extends grpc.MethodDefinition<projects_pb.IDRequest, projects_pb.Xorm> {
    path: "/projects.Xorms/getXormById";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<projects_pb.IDRequest>;
    requestDeserialize: grpc.deserialize<projects_pb.IDRequest>;
    responseSerialize: grpc.serialize<projects_pb.Xorm>;
    responseDeserialize: grpc.deserialize<projects_pb.Xorm>;
}

export const XormsService: IXormsService;

export interface IXormsServer extends grpc.UntypedServiceImplementation {
    getXormById: grpc.handleUnaryCall<projects_pb.IDRequest, projects_pb.Xorm>;
}

export interface IXormsClient {
    getXormById(request: projects_pb.IDRequest, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
    getXormById(request: projects_pb.IDRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
    getXormById(request: projects_pb.IDRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
}

export class XormsClient extends grpc.Client implements IXormsClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public getXormById(request: projects_pb.IDRequest, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
    public getXormById(request: projects_pb.IDRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
    public getXormById(request: projects_pb.IDRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: projects_pb.Xorm) => void): grpc.ClientUnaryCall;
}
