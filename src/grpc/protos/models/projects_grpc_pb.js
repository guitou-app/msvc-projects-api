// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var projects_pb = require('./projects_pb.js');

function serialize_projects_IDRequest(arg) {
  if (!(arg instanceof projects_pb.IDRequest)) {
    throw new Error('Expected argument of type projects.IDRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_projects_IDRequest(buffer_arg) {
  return projects_pb.IDRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_projects_Project(arg) {
  if (!(arg instanceof projects_pb.Project)) {
    throw new Error('Expected argument of type projects.Project');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_projects_Project(buffer_arg) {
  return projects_pb.Project.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_projects_Xorm(arg) {
  if (!(arg instanceof projects_pb.Xorm)) {
    throw new Error('Expected argument of type projects.Xorm');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_projects_Xorm(buffer_arg) {
  return projects_pb.Xorm.deserializeBinary(new Uint8Array(buffer_arg));
}


var ProjectsService = exports.ProjectsService = {
  isProjectExists: {
    path: '/projects.Projects/isProjectExists',
    requestStream: false,
    responseStream: false,
    requestType: projects_pb.IDRequest,
    responseType: projects_pb.Project,
    requestSerialize: serialize_projects_IDRequest,
    requestDeserialize: deserialize_projects_IDRequest,
    responseSerialize: serialize_projects_Project,
    responseDeserialize: deserialize_projects_Project,
  },
  getAllUserProjects: {
    path: '/projects.Projects/getAllUserProjects',
    requestStream: false,
    responseStream: true,
    requestType: projects_pb.IDRequest,
    responseType: projects_pb.Project,
    requestSerialize: serialize_projects_IDRequest,
    requestDeserialize: deserialize_projects_IDRequest,
    responseSerialize: serialize_projects_Project,
    responseDeserialize: deserialize_projects_Project,
  },
};

exports.ProjectsClient = grpc.makeGenericClientConstructor(ProjectsService);
var XormsService = exports.XormsService = {
  getXormById: {
    path: '/projects.Xorms/getXormById',
    requestStream: false,
    responseStream: false,
    requestType: projects_pb.IDRequest,
    responseType: projects_pb.Xorm,
    requestSerialize: serialize_projects_IDRequest,
    requestDeserialize: deserialize_projects_IDRequest,
    responseSerialize: serialize_projects_Xorm,
    responseDeserialize: deserialize_projects_Xorm,
  },
};

exports.XormsClient = grpc.makeGenericClientConstructor(XormsService);
