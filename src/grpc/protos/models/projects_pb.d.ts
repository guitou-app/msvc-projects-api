// package: projects
// file: projects.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class IDRequest extends jspb.Message { 
    getId(): string;
    setId(value: string): IDRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): IDRequest.AsObject;
    static toObject(includeInstance: boolean, msg: IDRequest): IDRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: IDRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): IDRequest;
    static deserializeBinaryFromReader(message: IDRequest, reader: jspb.BinaryReader): IDRequest;
}

export namespace IDRequest {
    export type AsObject = {
        id: string,
    }
}

export class Project extends jspb.Message { 
    getId(): string;
    setId(value: string): Project;
    getTitle(): string;
    setTitle(value: string): Project;
    getDescription(): string;
    setDescription(value: string): Project;

    hasAuthor(): boolean;
    clearAuthor(): void;
    getAuthor(): User | undefined;
    setAuthor(value?: User): Project;
    clearXormsList(): void;
    getXormsList(): Array<Xorm>;
    setXormsList(value: Array<Xorm>): Project;
    addXorms(value?: Xorm, index?: number): Xorm;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Project.AsObject;
    static toObject(includeInstance: boolean, msg: Project): Project.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Project, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Project;
    static deserializeBinaryFromReader(message: Project, reader: jspb.BinaryReader): Project;
}

export namespace Project {
    export type AsObject = {
        id: string,
        title: string,
        description: string,
        author?: User.AsObject,
        xormsList: Array<Xorm.AsObject>,
    }
}

export class User extends jspb.Message { 
    getId(): string;
    setId(value: string): User;
    getName(): string;
    setName(value: string): User;
    getEmail(): string;
    setEmail(value: string): User;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): User.AsObject;
    static toObject(includeInstance: boolean, msg: User): User.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): User;
    static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
    export type AsObject = {
        id: string,
        name: string,
        email: string,
    }
}

export class Xorm extends jspb.Message { 
    getId(): string;
    setId(value: string): Xorm;
    getTitle(): string;
    setTitle(value: string): Xorm;
    getLevel(): string;
    setLevel(value: string): Xorm;
    getProject(): string;
    setProject(value: string): Xorm;

    hasSettings(): boolean;
    clearSettings(): void;
    getSettings(): XormSettings | undefined;
    setSettings(value?: XormSettings): Xorm;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Xorm.AsObject;
    static toObject(includeInstance: boolean, msg: Xorm): Xorm.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Xorm, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Xorm;
    static deserializeBinaryFromReader(message: Xorm, reader: jspb.BinaryReader): Xorm;
}

export namespace Xorm {
    export type AsObject = {
        id: string,
        title: string,
        level: string,
        project: string,
        settings?: XormSettings.AsObject,
    }
}

export class XormSettings extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): XormSettings.AsObject;
    static toObject(includeInstance: boolean, msg: XormSettings): XormSettings.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: XormSettings, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): XormSettings;
    static deserializeBinaryFromReader(message: XormSettings, reader: jspb.BinaryReader): XormSettings;
}

export namespace XormSettings {
    export type AsObject = {
    }
}
