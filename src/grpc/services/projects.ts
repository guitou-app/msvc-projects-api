import { sendUnaryData, ServerUnaryCall, ServerWritableStream, UntypedHandleCall } from "@grpc/grpc-js";
import { getAllUserProjects, isProjectExists } from "../../services/projects";
import { fromDocumentToReply, mapDocumentToReply } from "../converters/project";
import { IProjectsServer } from "../protos/models/projects_grpc_pb";
import { IDRequest, Project } from "../protos/models/projects_pb";

export class Projects implements IProjectsServer {
  
  [method: string]: UntypedHandleCall;

  public async isProjectExists(call: ServerUnaryCall<IDRequest, Project>, callback: sendUnaryData<Project>) {
    const id: string = call.request.getId();

    try {
      const res: Project = fromDocumentToReply(await isProjectExists(id))
      callback(null, res);
    } catch (e) {
      console.log(Date.now(), 'Error');
      console.warn(e);
    }
  }

  public async getAllUserProjects(call: ServerWritableStream<IDRequest, Project>) {
    const userId: string = call.request.getId();

    try {
      const projects = await getAllUserProjects(userId);
      const replies = mapDocumentToReply(projects);
      for(const reply of replies) {
        call.write(reply);
      }
      call.end();
    } catch (e) {
      console.error(e);
    }
    
  }
}
