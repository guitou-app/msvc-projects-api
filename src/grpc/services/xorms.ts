import { handleUnaryCall, sendUnaryData, ServerUnaryCall, UntypedHandleCall } from '@grpc/grpc-js';
import { isValidObjectId, Types } from 'mongoose';
import { Xorm, XormDocument } from '../../models/xorm.model';
import { getXormById } from '../../services/xorms';
import { fromDocumentToReply } from '../converters/xorm';
import { IXormsServer } from '../protos/models/projects_grpc_pb';
import { IDRequest, Xorm as GrpcXorm } from '../protos/models/projects_pb';

export class Xorms implements IXormsServer {

  [method: string]: UntypedHandleCall;

  public async getXormById(call: ServerUnaryCall<IDRequest, GrpcXorm>, callback: sendUnaryData<GrpcXorm>) {
    const xormId = call.request.getId();

    try {
      const res: GrpcXorm = fromDocumentToReply(await getXormById(xormId))
      callback(null, res);
    } catch (e) {
      console.warn(e);
      callback(e as Error, null);
    }
  }
}