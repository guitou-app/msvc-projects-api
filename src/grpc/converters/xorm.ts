import { XormDocument } from "../../models/xorm.model";
import { Xorm } from "../protos/models/projects_pb";

export const fromDocumentToReply = (xorm: XormDocument): Xorm => {
  const reply: Xorm = new Xorm();
  reply.setId(xorm.id);
  reply.setTitle(xorm.title);
  reply.setLevel(xorm.level);
  reply.setProject(xorm.project.toString());
  // reply.setAuthor(author);
  // reply.setXormsList(xorm.xorms.map(xorm => {
  //   const x = xorm as XormDocument;

  //   return new Xorm()
  //     .setId(x.id)
  //     .setTitle(x.title)
  //     .setLevel(x.level)
  //     .setXorm(x.xorm)
  //   ;
  // }));

  return reply;
}

export const mapDocumentToReply = (xorms: XormDocument[]): Xorm[] => 
  xorms.map((p) => fromDocumentToReply(p))
