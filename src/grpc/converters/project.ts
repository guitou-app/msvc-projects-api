import { ProjectDocument } from "../../models/project.model";
import { XormDocument } from "../../models/xorm.model";
import { Project, User, Xorm } from "../protos/models/projects_pb";

export const fromDocumentToReply = (project: ProjectDocument): Project => {
  const reply: Project = new Project();
  reply.setId(project.id);
  reply.setTitle(project.title);
  reply.setDescription(project.description);

  const author = new User();
  author.setId(project.author.id);
  author.setName(project.author.name);
  author.setEmail(project.author.email);
  
  reply.setAuthor(author);
  reply.setXormsList(project.xorms.map(xorm => {
    const x = xorm as XormDocument;

    return new Xorm()
      .setId(x.id)
      .setTitle(x.title)
      .setLevel(x.level)
      .setProject(x.project)
    ;
  }));

  return reply;
}

export const mapDocumentToReply = (projects: ProjectDocument[]): Project[] => 
  projects.map((p) => fromDocumentToReply(p))
