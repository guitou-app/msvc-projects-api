import { Project, ProjectDocument } from "../models/project.model";

export const isProjectExists = async (projectId: string): Promise<ProjectDocument> => {
  let project: ProjectDocument;
  try {
    project = await Project.findById(projectId).populate('xorms') as ProjectDocument;
    return project;
  } catch (err) {
    return Promise.reject(err);
  }
}

export const getAllUserProjects = async (userId: string): Promise<ProjectDocument[]> => {
  let projects: ProjectDocument[] = [];

  try {
    projects = await Project.find({ "author.id": userId }).populate('xorms');
    return projects;
  } catch(e) {
    return Promise.reject(e);
  }
}

export const getProjectById = isProjectExists;
