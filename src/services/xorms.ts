import { isValidObjectId, Types } from "mongoose";
import { Xorm, XormDocument } from "../models/xorm.model";


// export const getXormById = async (xormId: string, language: string = ''): Promise<XormDocument> => {
interface QueryGetXormByIdOptions {
  language: string;
  populate: string[];
}
export const getXormById = async (xormId: string, options: Partial<QueryGetXormByIdOptions>): Promise<XormDocument> => {
  let xorm: XormDocument;
  let filter = {};
  let projection = {};

  if (isValidObjectId(xormId)) {
    filter = { _id: Types.ObjectId(xormId) };
  } else {
    filter = { "settings.customURLId": xormId };
  }

  const { populate, language } = options;
  if (language) {
    filter = {
      ...filter,
      [`translations.${language}`] : {
        '$exists': true
      }
    };
  }

  try {

    let xorm = await Xorm.findOne(filter, projection);
    if (populate) {
      for (let field of populate) {
        xorm = await xorm!.populate(field);
      }
    }

    if (language) {
      const xormTranslated = xorm?.translations.get(language);

      xorm!.title = xormTranslated.title;
      xorm!.description = xormTranslated.description;
      xorm!.xorm = xormTranslated.xorm;
    }

    return xorm!;
  } catch (err) {
    return Promise.reject(err);
  }
}