import { Request, Response } from 'express';
import { isValidObjectId, ObjectId, Types } from 'mongoose';
import { BadRequestError } from '../errors/bad-request-error';
import { DatabaseConnectionError } from '../errors/database-connection-error';
import { Project, ProjectDocument } from '../models/project.model';
import { Xorm, XormDocument, XormSection } from '../models/xorm.model';
import { getProjectById } from '../services/projects';
import { getXormById } from '../services/xorms';

const SettingsMap: Record<string, string> = {
  "customURLId": "settings.customURLId"
};

const create = async (req: Request, res: Response) => {
  let body = req.body;
  const { projectId } = req.params;
  let level: string;

  let xorm = new Xorm(body);
  xorm.project = projectId;
  xorm.xorm = {
    "section_0": {
      _params: { title: "Untitled Section", description: "", key: "section_0" },
      questions: {}
    }
  };
  
  const projectOne = await Project.findById(projectId);
  if (!projectOne) {
    throw new BadRequestError('The project referenced does not exists!');
  }

  try {
    if (!projectOne.settings.xorms.primary) {
      level = 'primary';
    } else {
      level = 'secondary';
    }
    xorm.level = level;
    await xorm.save();

    projectOne.xorms = [...projectOne.xorms, xorm.id];
    if (level === 'primary') {
      projectOne.settings.xorms.primary = xorm.id;
    }
    await projectOne.save();
  } catch (e) {
    throw new BadRequestError("Error when saving XORM");
  }

  return res.status(200).json(xorm);
}

const getAll = async (req: Request, res: Response) => {
  const { projectId } = req.params;
  let allXorms = [];

  const project = await Project.findByIdWithXorms(projectId); //.populate('xorms');
  if (!project) {
    throw new BadRequestError('The project referenced does not exists!');
  }

  allXorms = project.xorms.map((x:XormDocument) => {
    return {
      ...x.toJSON(),
      level: project.settings.xorms.primary == x.id ? 'primary' : 'secondary'
    }
  });

  return res.status(201).json(allXorms);
}

const getOne = async (req: Request, res: Response) => {
  const { projectId, xormId } = req.params;
  let xormOne: XormDocument | null;
  let data: Record<string, any>;

  try {
    xormOne = await Xorm.findOne({ _id: Types.ObjectId(xormId), project: projectId });
  } catch(e) {
    throw new DatabaseConnectionError('Error occured when fetching the Xorm');
  }

  if (!xormOne) {
    throw new BadRequestError('Either the project referenced or the Xorm requested does not exists!');
  }

  const projectOne = await Project.findById(projectId);
  if (!projectOne) {
    throw new BadRequestError('The project referenced does not exists!');
  }
  
  const { pKeyOrigin } = projectOne.settings.xorms;
  data = {
    ...xormOne.toJSON(),
    pKeyOrigin    
  }

  return res.status(200).json(data);
}

const getOneById = async (req: Request, res: Response) => {
  const { xormId } = req.params;
  const { language } = req.query;
  let xormOne: XormDocument | null;
  let data: Record<string, any>;
  xormOne = await getXormById(xormId, { language: language as string });

  if (!xormOne) {
    throw new BadRequestError('Either the project referenced or the Xorm requested does not exists!');
  }

  const projectOne = await Project.findById(xormOne.project);
  if (!projectOne) {
    throw new BadRequestError('The project referenced does not exists!');
  }
  
  const { pKeyOrigin } = projectOne.settings.xorms;
  data = {
    ...xormOne.toJSON(),
    pKeyOrigin,
    translations: undefined,
    settings: undefined,
    data: undefined,
    project: undefined,
  }

  return res.status(200).json(data);
}

const update = async function(req: Request, res: Response) {
  const { xormId } = req.params;
  const { body } = req;
  
  const set = {
    ...body.values
  }
  
  try {
    await Xorm.findByIdAndUpdate(xormId, { "$set": set });
  } catch (e) {
    throw new BadRequestError(`Error when update xorm: ${xormId}`);
  }

  return res.status(201).json();
}

const updateSettings = async (req:Request, res: Response) => {
  const { xormId } = req.params;
  const { body } = req;

  let currentXorm: XormDocument | null;
  try {
    currentXorm = await Xorm.findById(xormId)
  } catch (e) {
    throw new DatabaseConnectionError('Error occured when fetching the Xorm');
  }

  if (!currentXorm) {
    throw new BadRequestError('Either the project referenced or the Xorm requested does not exists!');
  }

  let settings: Record<string, any> = {};
  for (let key of Object.keys(body)) {
    const mapSetting: string = SettingsMap[key];
    settings = {
      ...settings,
      [mapSetting]: body[key]
    };
  }

  if (settings.hasOwnProperty("settings.customURLId")) {
    let customURLId = settings["settings.customURLId"];
    if (customURLId === -1) {
      customURLId = null;
    }

    try {
      await Xorm.findByIdAndUpdate(xormId, {
        "$set": {
          "settings.customURLId": customURLId
        }
      });
    } catch (e) {
      console.error(e);
      throw new BadRequestError(`Error when update xorm settings - default language: ${projectId} - ${(e as Error).message}`);
    }
  }
  return res.status(201).json();
}

const remove = async (req: Request, res: Response) => {
  const { projectId, xormId } = req.params;
  let xorm: XormDocument | null;
  let project: ProjectDocument | null;

  try {
    xorm = await Xorm.findById(xormId);
  } catch (err) {
    throw new DatabaseConnectionError(`Error occurred when fetching Xorm ${xormId}`);
  }
  if (!xorm) {
    throw new BadRequestError(`The Xorm [${xormId} requested does not exists!`);
  }

  try {
    project = await Project.findById(projectId);
  } catch (err) {
    throw new DatabaseConnectionError(`Error occured when fetching Project ${projectId}`);
  }
  if (!project) {
    throw new BadRequestError(`The Project [${projectId}] requested does not exists!`);
  }

  project.xorms = project.xorms
    .filter((x: Types.ObjectId | XormDocument) : x is Types.ObjectId => x != xorm!.id);
  if (project.settings.xorms.primary === xorm.id) {
    if (project.xorms.length) {
      project.settings.xorms.primary = project.xorms[0].toString(); // as ObjectId;
    } else {
      project.settings.xorms.primary = '';
    }
  }

  try {
    await xorm.delete()
  } catch (err) {
    throw new DatabaseConnectionError(`Error occured when deleting Xorm ${xormId}`);
  }

  try {
    await project.save()
  } catch (err) {
    throw new DatabaseConnectionError(`Error occured when saving Project ${projectId}`);
  }

  return res.status(201).json();
}

const setTranslation = async (req: Request, res: Response) => {
  const { projectId, xormId } = req.params;
  const { body } = req;
  const { language, data } = body;

  let currentProject: ProjectDocument | null;
  try {
    currentProject = await Project.findOne({ _id: Types.ObjectId(projectId), xorms: Types.ObjectId(xormId) })
  } catch (e) {
    throw new BadRequestError(`Error when finding project having this xormId: ${xormId} - ${projectId}`);
  }

  if (!currentProject) {
    throw new BadRequestError('The Xorm from that Project does not exists!');
  }

  let currentXorm: XormDocument | null;
  try {
    currentXorm = await Xorm.findOne({ _id: Types.ObjectId(xormId) })
  } catch (e) {
    throw new BadRequestError(`Error when finding xorm having this xormId: ${xormId}`);
  }

  try {
    const xormTranslation: Record<string, any> = {}

    currentXorm?.xorm.forEach((section: XormSection, sectionKey: string) => {
      const sectionTranslated = data['xorm'][sectionKey];

      const sectionTranslation: Record<string, any> = {
        _params: {
          ...section?._params,
          title: sectionTranslated['_params']['title'],
          description: sectionTranslated['_params']['description'],
        },
        questions: {},
      };
      for (let questionKey in section?.questions) {
        const questionTranslated = sectionTranslated.questions[questionKey];
        const question = section?.questions[questionKey];
        const { type } = question;
        if (
          type === 'multiple-choice' ||
          type === 'single-choice' ||
          type === 'single-choice-select' ||
          type === 'yes-no' ||
          type === 'yes-no-dont'
        ) {
          sectionTranslation.questions[questionKey] = {
            ...question,
            title: questionTranslated['title'],
            description: questionTranslated['description'],
            options: questionTranslated['options'] ?? question['options']
          }
        } else if (type === 'datatable') {
          sectionTranslation.questions[questionKey] = {
            ...question,
            title: questionTranslated['title'],
            description: questionTranslated['description'],
            cols: questionTranslated['cols'] ?? question['cols'],
            rows: questionTranslated['rows'] ?? question['rows'],
          }
        } else {
          sectionTranslation.questions[questionKey] = {
            ...question,
            title: questionTranslated['title'],
            description: questionTranslated['description'],
          }
        }
      }
      xormTranslation[sectionKey] = sectionTranslation;
    });

    currentXorm?.translations.set(language, {
      id: currentXorm?.id,
      title: data['title'],
      description: data['description'],
      xorm: xormTranslation,
    });

    await currentXorm?.save();
  } catch(e) {
    console.log(e);
    throw new BadRequestError(`Error occurred when set translation ${language} for project ${projectId} - ${JSON.stringify(data)}`);
  }

  return res.status(201).json();
}

const getAllAvailableLanguages = async (req: Request, res: Response) => {
  const { xormId } = req.params;
  const currentXorm: XormDocument | null  = await getXormById(xormId, { populate: ['project'] });
  const currentProject: ProjectDocument | null = currentXorm?.project as ProjectDocument;

  if (!currentXorm.get('translations')) {
    return res.status(200).json([]);
  }

  const languages = Array.from(currentXorm.translations.keys()).map(lang => ({
    language: lang,
    default: currentProject.settings.defaultLanguage === lang
  }));

  return res.status(200).json(languages);
}

const shouldTakeGeolocationPosition = async (req: Request, res: Response) => {
  const { xormId } = req.params;
  const currentXorm: XormDocument | null  = await getXormById(xormId, {});

  return res.status(200).json(currentXorm.get('settings.shouldTakeGeolocationPosition') ?? false);
}

const getCustomURLId = async (req: Request, res: Response) => {
  const { xormId } = req.params;
  const currentXorm: XormDocument | null  = await getXormById(xormId, {});

  return res.status(200).json(currentXorm.get('settings.customURLId'));
}

export {
  create,
  getAll,
  getOne,
  getOneById,
  update,
  updateSettings,
  remove,
  setTranslation,
  getAllAvailableLanguages,
  shouldTakeGeolocationPosition,
  getCustomURLId,
};
