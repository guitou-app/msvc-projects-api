import { Request, Response } from 'express';
import { Types } from 'mongoose';
import { BadRequestError } from '../errors/bad-request-error';
import { DatabaseConnectionError } from '../errors/database-connection-error';
import { Project, ProjectDocument, ProjectAttributes } from '../models/project.model';
import { Xorm, XormDocument } from '../models/xorm.model';

const SettingsMap: Record<string, string> = {
  "keyGeneration": "settings.xorms.pKeyOrigin",
  "primaryKey": "settings.xorms.primary",
  "collaborators": "settings.collaborators",
  "defaultLanguage": "settings.defaultLanguage",
  "geolocationPosition": "settings.xorms.geolocationPosition",
};

const getAll = async (req: Request, res: Response) => {
  let allProjects: ProjectDocument[] = [];

  try {
    allProjects = await Project.find({ "author.id": req.currentUser?.id });
  } catch(e) {
    throw new Error("Error when fetching projects.");
  }

  return res.status(200).json(allProjects);
}

const create = async (req: Request, res: Response) => {
  let { body } = req;

  let project = Project.build(body as ProjectAttributes);
  project.author = req.currentUser!;
  
  try {
    await project.save();
  } catch (e) {
    throw new BadRequestError("Error when saving XORM : " + e);
  }

  return res.status(201).json(project.toJSON())
}

const getOne = async function(req: Request, res: Response) {
  const { projectId } = req.params;
  let projectOne: ProjectDocument;

  try {
    projectOne = await Project.findById(projectId).populate('xorms') as ProjectDocument;
  } catch (e) {
    throw new BadRequestError(`Error when fetching one project ${projectId}`); 
  }

  return res.status(200).json(projectOne);
}

const update = async function (req: Request, res: Response) {
  const { body, params } = req;
  const { projectId } = params;

  try {
    await Xorm.findByIdAndUpdate(projectId, body);
  } catch (e) {
    throw new BadRequestError(`Error occured when updating project ${projectId}`);
  }

  return res.status(200).json(true);
}

const remove = async function (req: Request, res: Response) {
  const { projectId } = req.params;
  let projectOne;

  try {
    projectOne = await Project.findById(projectId);
  } catch {
    throw new BadRequestError(`Error occurred when fetching project ${projectId}`);
  }

  if (!projectOne) {
    throw new BadRequestError(`Sorry but the project ${projectId} does not exists`);
  }

  try {
    await Xorm.remove({_id: { "$in": projectOne.xorms }});
  } catch {
    throw new BadRequestError(`Error occured when deleting xorms of project ${projectId}`);
  }

  try {
    await projectOne.remove();
  } catch {
    throw new BadRequestError(`Error occured when delete project ${projectId}`);
  }

  return res.status(201).json();
}

const download = async function(req: Request, res: Response) {
  const projectId = req.params.projectId;
  let projectGenerated: ProjectGenerated;
  let projectOne

  try {
    projectOne = await Project.findById(projectId).populate('xorms');
  } catch (e) {
    throw new Error('Error when finding project to generated');
  }

  if (!projectOne) {
    return res.status(201).json({
      data: undefined
    });
  }

  projectGenerated = {
    id: projectOne.id,
    name: projectOne.title,
    description: projectOne.description,
    xorms: [],
    xormsDetails: {}
  };

  for (let _i = 0; _i < projectOne.xorms.length; _i++) {
    const xorm = projectOne.xorms[_i] as XormDocument;

    projectGenerated.xorms.push({
      id: xorm.id,
      title: xorm.title
    });

    projectGenerated.xormsDetails[xorm.id] = {
      ...xorm.xorm
    }
  }

  return res.status(201).json({
    data: projectGenerated
  });
}

const updateSettings = async function(req: Request, res: Response) {
  const { projectId } = req.params;
  let { body } = req;

  let currentProject: ProjectDocument | null;
  try {
    currentProject = await Project.findById(projectId)
  } catch (e) {
    throw new DatabaseConnectionError('Error occured when fetching the Xorm');
  }

  if (!currentProject) {
    throw new BadRequestError('Either the project referenced or the Xorm requested does not exists!');
  }

  let settings: Record<string, any> = {};
  for (let key of Object.keys(body)) {
    const mapSetting: string = SettingsMap[key];
    settings = {
      ...settings,
      [mapSetting]: body[key]
    };
    console.log("Key ... ", key, mapSetting, settings);
  }
  
  // try {
  //   // Update the settings
  //   await Project.findByIdAndUpdate(projectId, { "$set": settings });
  // } catch (e) {
  //   console.error(e);
  //   throw new BadRequestError(`Error when update xorm settings: ${projectId} - ${JSON.stringify(settings)}`);
  // }

  if (settings.hasOwnProperty("settings.defaultLanguage")) {
    const defaultLanguage = settings["settings.defaultLanguage"];
    try {
      // Add the language in the languages array if it's not yet
      await Project.findByIdAndUpdate(projectId, { 
        "$addToSet": { 
          "settings.languages": defaultLanguage
        },
        "$set": {
          "translations": {
            [defaultLanguage]: {
              "title": currentProject.title,
              "description": currentProject.description,
            }
          }
        }
      });
      // Add the default translations in Xorm
      for(let xormId of currentProject.xorms) {
        const currentXorm = await Xorm.findById(xormId);
        const xormTranslation: Record<string, any> = {}

        const { id, title, xorm } = currentXorm as XormDocument;
        currentXorm?.translations = new Map();
        currentXorm?.translations.set(defaultLanguage, {
          id,
          title,
          xorm,
        });

        await currentXorm?.save();
      }
    } catch (e) {
      console.error(e);
      throw new BadRequestError(`Error when update xorm settings - default language: ${projectId} - ${(e as Error).message}`);
    }
  }

  if (settings.hasOwnProperty("settings.xorms.geolocationPosition")) {
    const geolocationPosition = settings["settings.xorms.geolocationPosition"];
    try {
      await Project.findByIdAndUpdate(projectId, {
        "$set": {
          "settings.xorms.geolocationPosition": geolocationPosition
        }
      });
      for (let xormId of currentProject.xorms) {
        const id = (xormId as Types.ObjectId).toString();
        await Xorm.findByIdAndUpdate(xormId, {
          "$set": {
            "settings.shouldTakeGeolocationPosition": geolocationPosition
              .find((g: any) => g['xorm'] === id)['shouldTakeGeolocationPosition']
          }
        });
      }
    } catch (e) {
      console.error(e);
      throw new BadRequestError(`Error when update xorm settings - geolocation position: ${projectId} - ${(e as Error).message}`);
    }
  }

  return res.status(201).json();
}

const setTranslation = async function(req: Request, res: Response) {
  const { projectId } = req.params;
  const { body } = req;
  const { language, data } = body;

  try {
    await Project.findByIdAndUpdate(projectId, {
      "$addToSet": {
        "settings.languages": language
      },
      "$set": {
        [`translations.${language}`]: {
          ...data
        }
      }
    });
  } catch(e) {
    throw new BadRequestError(`Error occurred when set translation ${language} for project ${projectId} - ${JSON.stringify(data)}`);
  }

  return res.status(201).json();
}

export {
  getOne,
  getAll,
  create,
  update,
  remove,
  download,
  updateSettings,
  setTranslation
}

interface ProjectGenerated {
  id: string;
  name: string;
  description: string;
  xorms: {
    id: string;
    title: string;
  }[],
  xormsDetails: {[key: string]: any } // JSON }
}
