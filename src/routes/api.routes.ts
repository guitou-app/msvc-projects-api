import express, { Request, Response } from 'express';
import * as xormController from '../controllers/xorm.controller';

const router = express.Router();

router.route('/healthz')
  .get((req: Request, res: Response) => {
    return res.status(201).json({
      message: "it's working"
    });
  })
;

router.route('/x/:xormId')
  .get(xormController.getOneById)
;

router.route('/x/:xormId/translations')
  .get(xormController.getAllAvailableLanguages)
;

router.route('/x/:xormId/takeGeoPosition')
  .get(xormController.shouldTakeGeolocationPosition)
;

export { router as apiRouter }
