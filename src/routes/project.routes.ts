import express from 'express';
import { body } from 'express-validator';
import { validateRequest } from '../middlewares/validate-request';
import * as controller from '../controllers/projects.controller';
import { requiredAuth } from '../middlewares/required-auth';

const router = express.Router();

router.use(requiredAuth);

router.route('/')
  .get(controller.getAll)
  .post(
    [
      body('title')
        .notEmpty()
        .withMessage('Title must be given'),
      body('description')
        .notEmpty()
        .withMessage('Description must be give')
    ],
    validateRequest,
    controller.create
  );

router.route('/:projectId')
  .get(controller.getOne)
  .delete(controller.remove)
;

router.route('/:projectId/settings')
  .patch(controller.updateSettings)
;

router.route('/:projectId/translations')
  .post(controller.setTranslation)
;

export { router as projectRouter }
