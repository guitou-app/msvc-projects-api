import express from 'express';
import * as controller from '../controllers/xorm.controller';
import { requiredAuth } from '../middlewares/required-auth';

const router = express.Router();

router.use(requiredAuth);

router.route('/:projectId/xorms')
  .get(controller.getAll)
  .post(controller.create)
;

router.route('/:projectId/xorms/:xormId')
  .get(controller.getOne)
  .patch(controller.update)
  .delete(controller.remove)
;

router.route('/:projectId/xorms/:xormId/settings')
  .patch(controller.updateSettings)
;

router.route('/:projectId/xorms/:xormId/translations')
  .post(controller.setTranslation)
  .get(controller.getAllAvailableLanguages)
;

router.route('/:projectId/xorms/:xormId/customURLId')
  .get(controller.getCustomURLId)
;

export { router as xormRouter }
