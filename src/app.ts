import express from 'express';
import 'express-async-errors';
import cors from 'cors';
import morgan from 'morgan';

import { xormRouter } from './routes/xorm.routes';
import { projectRouter } from './routes/project.routes';
import { apiRouter } from './routes/api.routes';

import { errorHandler } from './middlewares/error-handler';
import { NotFoundError } from './errors/not-found-error';
import { currentUser } from './middlewares/current-user';

const PORT = process.env.PORT || 3000;
const MONGODB_URI = process.env.MONGODB_URI;

if (!MONGODB_URI) {
  throw new Error('MONGODB_URI must be defined!');
}

const app = express();
app.use(cors());
app.use(express.json());
app.use(morgan('combined'));

app.use(currentUser);

app.use(apiRouter);
app.use(xormRouter);
app.use(projectRouter);

app.all('*', async () => {
  throw new NotFoundError();
});

// Handle errors
app.use(errorHandler);

export default app;
