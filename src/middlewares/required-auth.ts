import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { BadRequestError } from "../errors/bad-request-error";
import { NotAuthenticatedError } from "../errors/not-authenticated-error";

interface UserPayload {
  id: string;
}


const requiredAuth = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  if (!req.currentUser) {
    throw new NotAuthenticatedError(`Authentication TOKEN required`);
  }

  next();
}

export { requiredAuth }