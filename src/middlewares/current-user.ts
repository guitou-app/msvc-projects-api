import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { User } from "../models/user";

declare global {
  namespace Express {
    interface Request {
      currentUser?: User;
    }
  }
}

const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authHeader: string = <string>req.headers["authorization"] ; 
  const token: string = authHeader && authHeader.split(" ")[1];

  if (!token) {
    req.currentUser = undefined;

    next();
    return 
  }

  let jwtPayload;
  const JWT_PUBLIC_KEY = process.env.JWT_PUBLIC_KEY;

  try {
    jwtPayload = jwt.verify(token, JWT_PUBLIC_KEY!) as User;
    req.currentUser = jwtPayload;
  } catch (error) {
    req.currentUser = undefined;
  }

  next();
}

export { currentUser }
