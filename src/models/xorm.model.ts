import mongoose, { Schema } from 'mongoose';
import { ProjectDocument } from './project.model';

export interface XormSectionQuestion {
  title: string;
  description: string;
  type: string;
  id: string;
  hint: string;
}
export interface XormSection {
  _params: {
    key: string;
    title: string;
    description: string;
  },
  questions: Record<string, XormSectionQuestion>
}
interface XormAttributes {
  title: string;
  description: string;
}

interface XormModel extends mongoose.Model<XormDocument> {
  build(attrs: XormAttributes): XormDocument
}

interface XormDocument extends mongoose.Document {
  title: string;
  description: string;
  project: string | ProjectDocument;
  author: string;
  createdAt: Date;
  level: string,
  pKeyOrigin: string,
  xorm: Record<string, XormSection>,
  settings: {
    displayData: {
      web: {
        columns: [any],
        data: []
      },
      mobile: {
        template: string,
        fields: any
      }
    },
    currentURLId: string,
    defaultLanguage: string
  },
  translations: Record<string, {
    title: string;
    description: string;
    xorm: Record<string, XormSection>;
  }>,
}

const XormSchema = new mongoose.Schema({
  title: {
    type: String,
    default: "Untitled xorm"
  },
  description: {
    type: String
  },
  level: {
    type: String,
    default: 'primary'
  },
  xorm: {
    type: Map,
    of: mongoose.Schema.Types.Mixed,
  }, // mongoose.Schema.Types.Mixed,
  settings: {
    displayData: {
      web: {
        columns: [mongoose.Schema.Types.Mixed],
        data: []
      },
      mobile: {
        template: String,
        fields: mongoose.Schema.Types.Mixed
      }
    },
    currentURLId: String,
    defaultLanguage: String,
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  translations: {
    type: Map,
    of: new Schema({
      title: String,
      description: String,
      xorm: {
        type: Map,
        of: mongoose.Schema.Types.Mixed
      }
    })
  },
  data: [mongoose.Schema.Types.ObjectId],
  author: mongoose.Schema.Types.ObjectId,
  createdAt: {
    type: Date,
    default: Date.now
  }
}, { strict: false });

XormSchema.statics.build = (attrs: XormAttributes) => {
  return new Xorm(attrs);
}

const Xorm = mongoose.model<XormDocument, XormModel>('Xorm', XormSchema);

export { Xorm, XormDocument, XormSchema };
