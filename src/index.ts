import mongoose from 'mongoose';
import serve from './grpc';
import app from './app';

const PORT = process.env.PORT || 3000;
const { MONGODB_URI, MONGODB_DBNAME, MONGODB_OPTIONS } = process.env;

if (!MONGODB_URI) {
  throw new Error('MONGODB_URI must be defined!');
}

if (!MONGODB_DBNAME) {
  throw new Error('MONGODB_DBNAME must be defined!');
}

if (!MONGODB_OPTIONS) {
  throw new Error('MONGODB_OPTIONS must be defined!');
}

// if (!process.env.RABBITMQ_URI) {
//   throw new Error('RABBITMQ_URI must be defined!');
// }

// if (!process.env.JWT_PUBLIC_KEY) {
//   throw new Error('JWT_PUBLIC must be defined!');
// }

const start = async () => {
  mongoose.set('toJSON', { virtuals: true });
  mongoose.set('debug', true);

  const uri = `${MONGODB_URI}/${MONGODB_DBNAME}?${MONGODB_OPTIONS}`;
  console.log(`MongoDB URI : ${uri}`);
  mongoose.connect(uri, (err) => {
    if (err) {
      throw new Error(`ERROR connecting DB ${uri}`);
    }
    console.log(`Mongoose connection open to ${uri}`);

    app.listen(PORT, () => {
      console.log(`Listening on port ${PORT}`);
    });

    serve();
  });

}

start();
